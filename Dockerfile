FROM node

# Create client directory inside container
WORKDIR /server

# Install all dependencies
#
# A wildcard is used to ensure both package.json AND
# package-lock.json are copied where available (npm@5+)
COPY package*.json ./

# When building code for production, comment the first
# option, and uncomment the second one
#
# TODO: separate code build configurations for
#       production/non-production branches so that
#       they do not overlap in the future
#
RUN npm install
# RUN npm ci --only=production

# Bundle  application source
COPY . ./

# Expose port to access application
#
# NOTE: this should point to an HTTP port in
#       the future, rather than an rbitrary
#       high-range port
#
EXPOSE 3250

# Startup command
CMD [ "npm", "start" ]
