var express = require('express');
const Users = require('../schemas/usersSchema');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 2;
const config = require('../config');


class UsersController {
    // Route to POST ADD one user
    async addUser(req, res) {
        let { type, firstname, lastname, email, password } = req.body;
        try {
            const userCreated = await Users.create({ type, firstname, lastname, email, password });
            res.send(userCreated);
        }
        catch (error) {
            res.send({ error });
        }
    }

    // Route to REMOVE user
    async removeUser(req, res) {
        let { _id } = req.body;
        try {
            const removed = await Users.deleteOne({ _id });
            res.send({ removed });
        } catch (error) {
            res.send({ error });
        }
    }
    //Route to UPDATE user
    async updateUser(req, res) {
        let { _id, type, firstname, lastname, email, password } = req.body;
        try {
            const objForUpdate = {}
            for (var key in req.body) {
                if (key !== '_id' && req.body.key !== null && req.body[key] !== "") {
                    objForUpdate[key] = req.body[key]
                }
            }
            const updated = await Users.updateOne({ _id }, objForUpdate);
            res.send({ updated });
        } catch (error) {
            res.send({ error });
        }
    }
    //Route to GET all users
    async getAllUsers(req, res) {
        try {
            const users = await Users.find({});
            res.send(users);
        } catch (error) {
            res.send({ error });
        }
    }

    // GET one user
    async getOneUser(req, res) {
        let { _id } = req.body;
        try {
            const user = await Users.findOne({ _id });
            res.send({ user });
        } catch (error) {
            res.send({ error });
        }
    }


    //registeration
    async register(req, res) {
        const { type, firstname, lastname, email, password, confirm_password } = req.body;
        if (!type || !firstname || !lastname || !email || !password || !confirm_password) return res.json({ ok: false, message: 'All field are required' });
        if (password !== confirm_password) return res.json({ ok: false, message: 'Passwords must match' });
        try {
            const user = await Users.findOne({ email })
            if (user) return res.json({ ok: false, message: 'Email is already in use!' });
            const hash = await bcrypt.hash(password, saltRounds)
    
            const newUser = {
                type,
                firstname,
                lastname,
                email,
                password: hash
            }
            const create = await Users.create(newUser)
            res.json({ ok: true, message: 'Thank you for registration! We sent you a confirmation email. Please follow the link to proceed.' })
        } catch (error) {
            res.json({ ok: false, error });
        }
    }

    //login 
    async login(req, res) {
        const { email, password } = req.body;
        if (!email || !password) res.json({ ok: false, message: 'All fields are required' });
        try {
            const user = await Users.findOne({ email });
            if (!user) return res.json({ ok: false, message: 'Please provide registered email' });
            const match = await bcrypt.compare(password, user.password);
            if (match) {
                const token = jwt.sign(user.toJSON(), config.secret, { expiresIn: 1000800 });
                res.json({ ok: true, message: 'Welcome back', token, email })
            } else return res.json({ ok: false, message: 'ERROR: invalid password entered: ' + password + ' and we expected: ' + user.password })

        } catch (error) {
            res.json({ ok: false, error })
            console.log(error);
        }
    }

    //token
    async verify_token(req, res) {
        const { token } = req.body;
        const decoded = jwt.verify(token, config.secret, (err, succ) => {
            err ? res.json({ ok: false, message: 'Something went wrong' }) : res.json({ ok: true, userType: succ.type, userID: succ._id, message: 'not sure what to put here' })
        });
    }
}


module.exports = new UsersController();
