const router = require('express').Router();
const controller = require('../controllers/activitiesController');

router.post('/activities/add', controller.addActivity);
router.delete('/activities/remove', controller.removeActivity);
router.post('/activities/update', controller.updateActivity);
router.get('/activities/all', controller.getAllActivities);
router.get('/activities/one', controller.getAllActivities);
router.post('/activities/find', controller.findActivities);

module.exports = router;