const router = require('express').Router();
const controller = require('../controllers/kidsController');

router.post('/kids/add', controller.addKid);
router.delete('/kids/remove', controller.removeKid);
router.post('/kids/update', controller.updateKid);
router.get('/kids/all', controller.getAllKids);
router.get('/kids/one', controller.getAllKids)

module.exports = router;