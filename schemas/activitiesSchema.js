const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const addressSubschema = Schema.addressSubschema;
const interestsSchema = Schema.interestsSchema;
const activitiesSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: "Please enter activity name!"
    },
    business: {
        type: ObjectId,
        ref: 'user',
        required: "Please enter business!"
    },
    location: {
        type: String
    },
    interests: [{
        type: ObjectId,
        ref: 'interest',
        required: "Please enter interests!"
    }],
    languages: {
        type: Array
    },
    description: {
        type: String
    },
    duration: {
        type: String
    },
    schedule: {
        type: String
    },
    pictures: [{ type: String }], // array of urls, the first pic should be the main one, the rest should show a gallery of thumbnails
    //this can also be called feedback from users, but should be premoderated i think..
    comments: [{
        type: ObjectId,
        ref: 'comment',
        required: false
    }]
});
module.exports = mongoose.model('activity', activitiesSchema);