import { Schema as _Schema, model } from 'mongoose';
const Schema = _Schema;
const addressesSubschema = new Schema({
    street: {
        type: String,
        unique: false,
        required: "Please enter street name!"
    },
    number: {
        type: String,
        unique: false,
        required: "Please enter street number!"
    },
    city: {
        type: String,
        required: "Please enter city name!"
    },
    country: {
        type: String,
        required: "Please enter country name!"
    },
    zip: {
        type: String,
    }
});
export default model('address', addressesSubschema);