import { Schema as _Schema, model } from 'mongoose';
const Schema = _Schema;
const ObjectId = Schema.Types.ObjectId; // here we can change ObjectID to ObjectName?
const bookingsSchema = new Schema({
    order_date: {
        type: date,
        unique: false,
        required: "Date was not generated!"
    },
    activity_date: {
        type: date,
        unique: false,
        required: "Please select date!"
    },
    user: {
        type: ObjectId,
        ref: 'user',
        required: "User ID was not provided"
    },
    activity: {
        type: ObjectId,
        ref: 'activity',
        required: true
    },
    paid: {
        type: Boolean,
        default: false
    }
});
export default model('booking', bookingsSchema);