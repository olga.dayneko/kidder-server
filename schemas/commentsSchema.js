const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const acommentsSubschema = new Schema({
    text: {
        type: String,
        unique: true,
        required: "Please enter comment text!"
    },
    username: {
        type: ObjectId,
        ref: 'user',
        required: "Username cannot be empty!"
    },
    time: [{
        type: time,
        required: "Time cannot be empty"
    }]
});
module.exports = mongoose.model('comment', acommentsSubschema);