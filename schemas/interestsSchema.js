const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const interestsSubschema = new Schema(
    {
        name: {
            type: String,
            unique: true
        }
    });
module.exports = mongoose.model('interest', interestsSubschema);