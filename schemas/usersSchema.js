const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId; // here we can change ObjectID to ObjectName?
const usersSchema = new Schema({
    type: {
        type: String,
        required: "User type must be set!",
        enum: ['admin', 'parent', 'business']
    },
    firstname: {
        type: String,
        unique: false,
        required: "Please enter name!"
    },
    lastname: {
        type: String,
        unique: false,
        required: "Please enter lastname!"
    },
    email: {
        type: String,
        unique: "User with this email already exists!",
        required: "Please provide your email!"
    },
    password: {
        type: String,
        required: "Please provide your password!"
    },
    default_location: {
        type: ObjectId,
        ref: 'address',
        required: false
    },
    kids: [{
        type: ObjectId,
        ref: 'kid',
        required: false
    }],
    activities: [{
        type: ObjectId,
        ref: 'activity',
        required: false
    }],
    profile_picture: { type: String },
});
module.exports = mongoose.model('user', usersSchema);